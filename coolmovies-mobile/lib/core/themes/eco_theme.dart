import 'package:flutter/material.dart';

class EcoTheme {
  static ThemeData get lightTheme => ThemeData(
        primarySwatch: Colors.deepPurple,
      );

  static ThemeData get darkTheme => ThemeData(
        brightness: Brightness.dark,
      );
}
