import 'package:flutter/material.dart';

import '../../data/models/review_model.dart';

class ReviewWidget extends StatelessWidget {
  final ReviewModel review;
  final VoidCallback? onTap;
  final bool expanded;
  const ReviewWidget({
    Key? key,
    required this.review,
    this.onTap,
    this.expanded = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: InkWell(
          borderRadius: BorderRadius.circular(4.0),
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.deepPurple.shade400,
                      foregroundColor: Colors.white,
                      child: const Icon(Icons.person),
                    ),
                    const SizedBox(width: 8.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(review.userName ?? ''),
                        const SizedBox(height: 4.0),
                        Row(
                          children: [
                            Row(
                              children: List.generate(
                                review.rating,
                                (index) => const Icon(
                                  Icons.star,
                                  size: 16.0,
                                  color: Colors.amber,
                                ),
                              ),
                            ),
                            const SizedBox(width: 4.0),
                            Text(
                              review.rating.toStringAsFixed(1),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
                const Divider(),
                Text(
                  review.title,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 4.0),
                Row(
                  children: [
                    Expanded(
                      child: expanded
                          ? Text(
                              review.body,
                            )
                          : Text(
                              review.body,
                              maxLines: 4,
                              overflow: TextOverflow.fade,
                            ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
