// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:coolmovies/core/widgets/app_header.dart';
import 'package:flutter/material.dart';

import '../../data/models/movie_model.dart';

class MovieWidget extends StatelessWidget {
  final MovieModel movie;
  final VoidCallback? onTap;
  final VoidCallback? onBackButtonPressed;
  final bool expanded;
  const MovieWidget({
    Key? key,
    required this.movie,
    this.onTap,
    this.onBackButtonPressed,
    this.expanded = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        children: [
          SizedBox(
            width: expanded
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width * 0.90,
            height: expanded
                ? MediaQuery.of(context).size.height * 0.70
                : MediaQuery.of(context).size.height * 0.30,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Image.network(
                movie.imgUrl,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: const EdgeInsets.all(8.0),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(16.0),
                  bottomRight: Radius.circular(16.0),
                ),
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black,
                    Colors.transparent,
                  ],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title,
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall
                        ?.apply(color: Colors.white),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Icon(
                        Icons.calendar_month_outlined,
                        size: 16.0,
                        color: Colors.white,
                      ),
                      Text(
                        movie.releaseDate.split('-')[0],
                        style: Theme.of(context)
                            .textTheme
                            .labelSmall
                            ?.apply(color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          expanded
              ? Positioned(
                  height: 120.0,
                  top: 0,
                  left: 0,
                  right: 0,
                  child: AppHeader(
                    title: movie.title,
                    onPressed:
                        onBackButtonPressed ?? () => Navigator.pop(context),
                  ),
                )
              : const SizedBox.shrink(),
        ],
      ),
    );
  }
}
