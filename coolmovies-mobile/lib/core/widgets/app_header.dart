// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final VoidCallback? onPressed;
  const AppHeader({
    Key? key,
    required this.title,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 48.0),
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF10002b),
                Color(0xD210002B),
                Color(0x8710002B),
                Color(0x003c096c),
              ],
            ),
          ),
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline6?.apply(
                  color: Colors.white70,
                ),
          ),
        ),
        onPressed != null
            ? Positioned(
                top: 0,
                bottom: 0,
                left: 16.0,
                child: InkWell(
                  onTap: onPressed,
                  child: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white70,
                  ),
                ),
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(120.0);
}
