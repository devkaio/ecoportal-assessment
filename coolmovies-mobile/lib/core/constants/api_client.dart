import 'dart:io';

import 'package:graphql_flutter/graphql_flutter.dart';

final authLink = AuthLink(
  getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
);

final httpLink = HttpLink(
  Platform.isAndroid
      ? 'http://10.0.2.2:5001/graphql'
      : 'http://localhost:5001/graphql',
);

final link = authLink.concat(httpLink);

final cache = GraphQLCache(
  store: HiveStore(),
);

class ApiClient {
  ApiClient._();

  static final _instance = ApiClient._();

  factory ApiClient() => _instance;

  final _client = GraphQLClient(
    defaultPolicies: DefaultPolicies(
      mutate: Policies(
        fetch: FetchPolicy.cacheAndNetwork,
      ),
      query: Policies(
        fetch: FetchPolicy.cacheAndNetwork,
      ),
    ),
    link: link,
    cache: cache,
  );

  GraphQLClient get instance => _client;
}
