import 'package:flutter/material.dart';

import '../../features/home/home_page.dart';
import '../../features/movie/movie_page.dart';

class Routes {
  static Route<Widget>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => const HomePage(title: 'Coolmovies'),
        );
      case '/movie-detail':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => MoviePage(
            args: settings.arguments as Map<String, dynamic>?,
          ),
        );
      default:
        return null;
    }
  }
}
