import 'dart:convert';

class ReviewModel {
  final String? id;
  final String body;
  final int rating;
  final String title;
  final String? movieId;
  final String? userName;
  final String? userId;

  ReviewModel({
    this.id,
    required this.body,
    required this.rating,
    required this.title,
    this.movieId,
    this.userName,
    this.userId,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'body': body,
      'rating': rating,
      'title': title,
      'movieId': movieId,
      'userId': userId,
      'userName': userName,
    };
  }

  factory ReviewModel.fromMap(Map<String, dynamic> map) {
    return ReviewModel(
      id: map['id'] != null ? map['id'] as String : null,
      body: map['body'] as String,
      rating: map['rating'] as int,
      title: map['title'] as String,
      movieId: map['movieId'] != null ? map['movieId'] as String : null,
      userName: map['userByUserReviewerId']['name'] != null
          ? map['userByUserReviewerId']['name'] as String
          : '',
      userId: map['userByUserReviewerId']['id'] != null
          ? map['userByUserReviewerId']['id'] as String
          : '',
    );
  }

  String toJson() => json.encode(toMap());

  factory ReviewModel.fromJson(String source) =>
      ReviewModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
