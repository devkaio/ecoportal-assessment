import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'review_model.dart';

class MovieModel {
  final String? id;
  final String imgUrl;
  final String movieDirectorId;
  final String userCreatorId;
  final String title;
  final String releaseDate;
  final String nodeId;
  final List<ReviewModel>? movieReviewsByMovieId;
  MovieModel({
    this.id,
    required this.imgUrl,
    required this.movieDirectorId,
    required this.userCreatorId,
    required this.title,
    required this.releaseDate,
    required this.nodeId,
    this.movieReviewsByMovieId,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'imgUrl': imgUrl,
      'movieDirectorId': movieDirectorId,
      'userCreatorId': userCreatorId,
      'title': title,
      'releaseDate': releaseDate,
      'nodeId': nodeId,
      'movieReviewsByMovieId':
          movieReviewsByMovieId?.map((x) => x.toMap()).toList(),
    };
  }

  factory MovieModel.fromMap(Map<String, dynamic> map) {
    return MovieModel(
      id: map['id'] != null ? map['id'] as String : null,
      imgUrl: map['imgUrl'] as String,
      movieDirectorId: map['movieDirectorId'] as String,
      userCreatorId: map['userCreatorId'] as String,
      title: map['title'] as String,
      releaseDate: map['releaseDate'] as String,
      nodeId: map['nodeId'] as String,
      movieReviewsByMovieId: map['movieReviewsByMovieId']['edges'] != null
          ? List<ReviewModel>.from(
              (map['movieReviewsByMovieId']['edges'] as List).map<ReviewModel?>(
                (x) => ReviewModel.fromMap((x as Map<String, dynamic>)['node']),
              ),
            )
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory MovieModel.fromJson(String source) =>
      MovieModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool operator ==(covariant MovieModel other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.imgUrl == imgUrl &&
        other.movieDirectorId == movieDirectorId &&
        other.userCreatorId == userCreatorId &&
        other.title == title &&
        other.releaseDate == releaseDate &&
        other.nodeId == nodeId &&
        listEquals(other.movieReviewsByMovieId, movieReviewsByMovieId);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        imgUrl.hashCode ^
        movieDirectorId.hashCode ^
        userCreatorId.hashCode ^
        title.hashCode ^
        releaseDate.hashCode ^
        nodeId.hashCode ^
        movieReviewsByMovieId.hashCode;
  }
}
