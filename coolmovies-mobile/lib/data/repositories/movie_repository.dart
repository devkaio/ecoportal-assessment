import 'package:graphql_flutter/graphql_flutter.dart';

import '../../core/constants/api_client.dart';
import '../models/movie_model.dart';
import '../models/review_model.dart';

abstract class MovieRepository {
  Future<List<MovieModel>> getAllMovies();
  Future<List<ReviewModel>> getAllReviews();
}

class HomeRepositoryImpl implements MovieRepository {
  HomeRepositoryImpl();
  @override
  Future<List<MovieModel>> getAllMovies() async {
    try {
      final result = await ApiClient().instance.query<List<MovieModel>>(
            QueryOptions(
              document: gql("""
          query AllMovies {
            allMovies {
              nodes {
                id
                imgUrl
                movieDirectorId
                userCreatorId
                title
                releaseDate
                nodeId
                movieReviewsByMovieId {
                  edges {
                    node {
                      id
                      body
                      rating
                      title
                      movieId
                      userByUserReviewerId {
                        id
                        name
                      }
                    }
                  }
                }
              }
            }
          }
        """),
            ),
          );
      if (result.hasException) {
        throw Exception("error");
      } else {
        final data = List.from(result.data?['allMovies']['nodes']);
        final movieList = data.map((e) => MovieModel.fromMap(e)).toList();
        return movieList;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ReviewModel>> getAllReviews() async {
    try {
      final result = await ApiClient().instance.query<List<ReviewModel>>(
            QueryOptions(
              document: gql("""
                query AllReviews {
                  allMovieReviews(orderBy: RATING_DESC) {
                    nodes {
                      id
                      body
                      rating
                      title
                      movieId
                      userByUserReviewerId {
                        name
                        id
                      }
                    }
                  }
                }
            """),
            ),
          );
      if (result.hasException) {
        throw Exception("error");
      } else {
        final data = List.from(result.data?['allMovieReviews']['nodes']);
        final reviewList = data.map((e) => ReviewModel.fromMap(e)).toList();
        return reviewList;
      }
    } catch (e) {
      rethrow;
    }
  }
}
