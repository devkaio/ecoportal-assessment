import 'package:graphql_flutter/graphql_flutter.dart';

import '../../core/constants/api_client.dart';
import '../models/review_model.dart';

abstract class ReviewRepository {
  Future<List<ReviewModel>> createReview({
    required String userName,
    required ReviewModel review,
  });

  Future<List<ReviewModel>> updateReview({
    required ReviewModel review,
  });
}

class ReviewRepositoryImpl implements ReviewRepository {
  ReviewRepositoryImpl();

  @override
  Future<List<ReviewModel>> createReview({
    required String userName,
    required ReviewModel review,
  }) async {
    try {
      final client = ApiClient().instance;
      final createUserResponse = await client.mutate(
        MutationOptions(
          document: gql(
            """
            mutation CreateUserMutation{
                createUser(input: { user: { name: "$userName" } }) {
                  user {
                    id
                    name
                  }
                }
              }
            """,
          ),
        ),
      );

      if (createUserResponse.data?['createUser']['user']['id'] != null) {
        final userId = createUserResponse.data?['createUser']['user']['id'];

        final createReviewResponse = await client.mutate(
          MutationOptions(
            document: gql("""
                  mutation CreateMovieMutation {
                    createMovieReview(
                      input: {
                        movieReview: {
                          title: "${review.title}"
                          movieId: "${review.movieId}"
                          userReviewerId: "$userId"
                          rating: ${review.rating}
                          body: "${review.body}"
                        }
                      }
                    ) {
                      clientMutationId
                      query {
                      allMovieReviews {
                        edges {
                          node {
                            id
                            movieId
                            rating
                            title
                            body
                            userByUserReviewerId {
                              name
                              id
                            }
                          }
                        }
                      }
                    }
                    }
                  }
              """),
          ),
        );

        if (createReviewResponse.hasException) {
          throw Exception("error");
        } else {
          final data = List.from(createReviewResponse.data?['createMovieReview']
              ['query']['allMovieReviews']['edges']);
          final reviewList =
              data.map((e) => ReviewModel.fromMap(e['node'])).toList();
          return reviewList;
        }
      }
      throw Exception('error');
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ReviewModel>> updateReview({
    required ReviewModel review,
  }) async {
    try {
      final result = await ApiClient().instance.mutate(
            MutationOptions(
              document: gql(
                """
                mutation UpdateMovieReview {
                  updateMovieReviewById(
                    input: {
                      movieReviewPatch: {
                        title: "${review.title}"
                        rating: ${review.rating}
                        body: "${review.body}"
                        userReviewerId: "${review.userId}"
                      }
                      id: "${review.id}"
                    }
                  ) {
                    clientMutationId
                    query {
                      allMovieReviews {
                        edges {
                          node {
                            id
                            movieId
                            rating
                            title
                            body
                            userByUserReviewerId {
                              name
                              id
                            }
                          }
                        }
                      }
                    }
                  }
                }
              """,
              ),
            ),
          );
      if (result.hasException) {
        throw Exception("error");
      } else {
        final data = List.from(result.data?['updateMovieReviewById']['query']
            ['allMovieReviews']['edges']);
        final reviewList =
            data.map((e) => ReviewModel.fromMap(e['node'])).toList();
        return reviewList;
      }
    } catch (e) {
      rethrow;
    }
  }
}
