import 'package:flutter/material.dart';

import 'core/routes/routes.dart';
import 'core/themes/eco_theme.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: Routes.onGenerateRoute,
      title: 'ecoPortal Mobile Test',
      theme: EcoTheme.lightTheme,
      darkTheme: EcoTheme.darkTheme,
    );
  }
}
