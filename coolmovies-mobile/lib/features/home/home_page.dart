import 'package:flutter/material.dart';

import '../../core/widgets/app_header.dart';
import '../../core/widgets/movie_widget.dart';
import '../../core/widgets/review_widget.dart';
import '../../data/repositories/movie_repository.dart';
import 'home_controller.dart';
import 'home_state.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = HomeController(HomeRepositoryImpl());

  @override
  void initState() {
    super.initState();
    controller.getAllMovies();
    controller.getAllReviews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppHeader(
        title: widget.title,
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          AnimatedBuilder(
            animation: controller.state,
            builder: (context, child) {
              switch (controller.state.value.runtimeType) {
                case HomeStateLoading:
                  return const Center(
                    child: LinearProgressIndicator(),
                  );
                case HomeStateSuccess:
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: controller.movies
                          .map(
                            (movie) => Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: MovieWidget(
                                movie: movie,
                                onTap: () async {
                                  await Navigator.pushNamed(
                                    context,
                                    '/movie-detail',
                                    arguments: {
                                      "reviews": controller.reviews,
                                      "movie": movie,
                                    },
                                  );
                                  controller.getAllReviews();
                                },
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  );
                case HomeStateError:
                  return const Center(
                    child: Text(
                        "An unkown error has occured. Please try again later."),
                  );

                default:
                  return const SizedBox.shrink();
              }
            },
          ),
          Text(
            "Top Reviews",
            style: Theme.of(context).textTheme.headline6,
            textAlign: TextAlign.center,
          ),
          AnimatedBuilder(
            animation: controller.state,
            builder: (context, child) {
              if (controller.reviews.isEmpty) {
                return const Center(
                  child: Text("There are no reviews yet."),
                );
              }
              switch (controller.state.value.runtimeType) {
                case HomeStateLoading:
                  return const SizedBox(
                    height: 120.0,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                case HomeStateError:
                  return const Center(
                    child: Text("An error has ocurred while fetching reviews"),
                  );
                case HomeStateSuccess:
                  return Column(
                    children: controller.reviews
                        .map(
                          (e) => ReviewWidget(
                            review: e,
                            onTap: () async {
                              await Navigator.pushNamed(
                                context,
                                '/movie-detail',
                                arguments: {
                                  "reviews": controller.reviews,
                                  "movie": controller
                                      .getSelectedMovieByReview(e.movieId),
                                },
                              );

                              controller.getAllReviews();
                            },
                          ),
                        )
                        .toList(),
                  );
                default:
                  return const SizedBox.shrink();
              }
            },
          ),
        ],
      ),
    );
  }
}
