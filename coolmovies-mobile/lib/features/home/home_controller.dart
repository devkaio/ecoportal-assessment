import 'package:flutter/cupertino.dart';

import '../../data/models/movie_model.dart';
import '../../data/models/review_model.dart';
import '../../data/repositories/movie_repository.dart';
import 'home_state.dart';

class HomeController {
  final MovieRepository repository;
  HomeController(this.repository);

  final ValueNotifier<HomeState> state = ValueNotifier(HomeStateInitial());

  List<MovieModel> _movieList = [];
  List<ReviewModel> _reviewList = [];

  List<MovieModel> get movies => _movieList;

  List<ReviewModel> get reviews => _reviewList;

  void getAllMovies() async {
    state.value = HomeStateLoading();

    try {
      _movieList = await repository.getAllMovies();

      state.value = HomeStateSuccess();
    } catch (e) {
      state.value = HomeStateError();
    }
  }

  void getAllReviews() async {
    state.value = HomeStateLoading();

    try {
      _reviewList = await repository.getAllReviews();

      state.value = HomeStateSuccess();
    } catch (e) {
      state.value = HomeStateError();
    }
  }

  MovieModel getSelectedMovieByReview(String? id) {
    return movies.firstWhere((element) => element.id == id);
  }
}
