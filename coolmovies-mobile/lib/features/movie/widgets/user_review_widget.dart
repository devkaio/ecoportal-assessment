import 'package:flutter/material.dart';

import '../../../data/models/review_model.dart';
import '../movie_controller.dart';

class UserReviewWidget {
  UserReviewWidget._();

  static Future<void> postReview(
    BuildContext context, {
    Map<String, dynamic>? args,
  }) async {
    final formKey = GlobalKey<FormState>();
    final ReviewModel? review = args?['review'];
    String reviewerName = review?.userName ?? '';
    String reviewTitle = review?.title ?? '';
    String reviewBody = review?.body ?? '';
    int reviewRating = review?.rating ?? 1;
    final MovieController? controller = args?['controller'];

    await showModalBottomSheet<void>(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Form(
              key: formKey,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  TextFormField(
                    initialValue: reviewerName,
                    enabled: review?.userId != null ? false : true,
                    decoration: InputDecoration(
                      labelText: review?.userId != null ? null : "Your Name",
                    ),
                    onChanged: (value) => reviewerName = value,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      return value!.isEmpty
                          ? "This field cannot be empty"
                          : null;
                    },
                  ),
                  const SizedBox(height: 8.0),
                  TextFormField(
                    initialValue: reviewTitle,
                    decoration: const InputDecoration(
                      labelText: "Your Review Title",
                    ),
                    onChanged: (value) => reviewTitle = value,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      return value!.isEmpty
                          ? "This field cannot be empty"
                          : null;
                    },
                  ),
                  const SizedBox(height: 8.0),
                  StatefulBuilder(
                    builder: (context, setState) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Slider(
                            divisions: 4,
                            max: 5.0,
                            min: 1.0,
                            label: reviewRating.toStringAsFixed(0),
                            value: reviewRating.toDouble(),
                            onChanged: (value) {
                              setState(() => reviewRating = value.toInt());
                            },
                          ),
                          const Text("Review from 1 to 5"),
                        ],
                      );
                    },
                  ),
                  TextFormField(
                    initialValue: reviewBody,
                    maxLines: 5,
                    textInputAction: TextInputAction.done,
                    decoration: const InputDecoration(
                      labelText: "Your review",
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                    onChanged: (value) => reviewBody = value,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      return value!.isEmpty
                          ? "This field cannot be empty"
                          : null;
                    },
                  ),
                  const SizedBox(height: 16.0),
                  ElevatedButton(
                    onPressed: () {
                      if (formKey.currentState?.validate() ?? false) {
                        controller?.postReview(
                          review: ReviewModel(
                            id: review?.id,
                            title: reviewTitle,
                            body: reviewBody,
                            rating: reviewRating,
                            movieId: controller.movie?.id,
                            userName: reviewerName,
                            userId: review?.userId,
                          ),
                        );
                      }
                      Navigator.pop(context);
                    },
                    child: review?.userId != null
                        ? const Text("Update Review")
                        : const Text("Post Review"),
                  ),
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text("Cancel"))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
