abstract class MovieState {}

class MovieStateInitial extends MovieState {}

class MovieStateLoading extends MovieState {}

class MovieStateSuccess extends MovieState {}

class MovieStateError extends MovieState {}
