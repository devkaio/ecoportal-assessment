import 'package:flutter/foundation.dart';

import '../../data/models/movie_model.dart';
import '../../data/models/review_model.dart';
import '../../data/repositories/review_repository.dart';
import 'movie_state.dart';

class MovieController {
  final ReviewRepository _repository;
  MovieController(this._repository);

  ValueNotifier<MovieState> movieState = ValueNotifier(MovieStateInitial());

  ValueNotifier<bool> writeReviewButtonState = ValueNotifier(true);

  List<ReviewModel> _reviews = [];
  MovieModel? _movie;

  List<ReviewModel> get reviewList => _reviews;

  MovieModel? get movie => _movie;

  void setMovieReviews(List<ReviewModel>? reviews) => _reviews =
      reviews?.where((element) => element.movieId == movie?.id).toList() ?? [];
  void selectedMovie(MovieModel? movie) => _movie = movie;

  void showButton(bool value) {
    writeReviewButtonState.value = value;
  }

  void postReview({
    required ReviewModel review,
  }) async {
    movieState.value = MovieStateLoading();

    if (review.userId == null) {
      try {
        final result = await _repository.createReview(
          userName: review.userName!,
          review: review,
        );
        _reviews =
            result.where((element) => element.movieId == movie?.id).toList();
        movieState.value = MovieStateSuccess();
      } catch (e) {
        movieState.value = MovieStateError();
      }
    } else {
      try {
        final result = await _repository.updateReview(
          review: review,
        );
        _reviews =
            result.where((element) => element.movieId == movie?.id).toList();
        movieState.value = MovieStateSuccess();
      } catch (e) {
        movieState.value = MovieStateError();
      }
    }
  }
}
