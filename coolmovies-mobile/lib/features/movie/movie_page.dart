import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../../core/widgets/movie_widget.dart';
import '../../core/widgets/review_widget.dart';
import '../../data/repositories/review_repository.dart';
import 'movie_controller.dart';
import 'movie_state.dart';
import 'widgets/user_review_widget.dart';

class MoviePage extends StatefulWidget {
  final Map<String, dynamic>? args;

  const MoviePage({
    Key? key,
    this.args,
  }) : super(key: key);

  @override
  State<MoviePage> createState() => _MoviePageState();
}

class _MoviePageState extends State<MoviePage> {
  final MovieController controller = MovieController(ReviewRepositoryImpl());

  final _duration = const Duration(milliseconds: 300);
  @override
  void initState() {
    super.initState();
    controller.selectedMovie(widget.args?['movie']);
    controller.setMovieReviews(widget.args?['reviews']);
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<UserScrollNotification>(
      onNotification: (notification) {
        final ScrollDirection direction = notification.direction;
        switch (direction) {
          case ScrollDirection.forward:
            controller.showButton(true);
            break;
          case ScrollDirection.reverse:
            controller.showButton(false);
            break;
          case ScrollDirection.idle:
            break;
        }
        return true;
      },
      child: Scaffold(
        body: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Column(
            children: [
              MovieWidget(
                movie: controller.movie!,
                expanded: true,
                onBackButtonPressed: () {
                  Navigator.pop(context);
                },
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "User Reviews",
                  style: Theme.of(context).textTheme.headline6,
                  textAlign: TextAlign.center,
                ),
              ),
              AnimatedBuilder(
                animation: controller.movieState,
                builder: (context, _) {
                  if (controller.reviewList.isEmpty) {
                    return const Center(
                      child: Text("There are no reviews yet."),
                    );
                  }
                  switch (controller.movieState.value.runtimeType) {
                    case MovieStateLoading:
                      return const Center(
                        child: LinearProgressIndicator(),
                      );
                    case MovieStateInitial:
                    case MovieStateSuccess:
                      return Column(
                        children: controller.reviewList
                            .map(
                              (e) => ReviewWidget(
                                review: e,
                                expanded: true,
                                onTap: () async {
                                  await UserReviewWidget.postReview(
                                    context,
                                    args: {
                                      'review': e,
                                      'controller': controller,
                                    },
                                  );
                                },
                              ),
                            )
                            .toList(),
                      );
                    case MovieStateError:
                      return const Center(
                        child: Text(
                            "An error has occurred while fetching reviews."),
                      );
                    default:
                      return const SizedBox.shrink();
                  }
                },
              )
            ],
          ),
        ),
        floatingActionButton: AnimatedBuilder(
          animation: controller.writeReviewButtonState,
          builder: (context, child) {
            return AnimatedSlide(
              duration: _duration,
              offset: controller.writeReviewButtonState.value
                  ? Offset.zero
                  : const Offset(0, 2),
              child: AnimatedOpacity(
                duration: _duration,
                opacity: controller.writeReviewButtonState.value ? 1 : 0,
                child: FloatingActionButton.extended(
                  onPressed: () async {
                    await UserReviewWidget.postReview(
                      context,
                      args: {
                        "controller": controller,
                        "movie": controller.movie,
                      },
                    );
                  },
                  label: const Text("Write Review"),
                  icon: const Icon(Icons.edit),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
