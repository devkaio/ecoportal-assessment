# Coolmovies Mobile Challenge

### EcoPortal Assessment build with Flutter and GraphQL API.

## Features
- [x] List all the available movies, showing at least the title
- [x] Tapping on a movie must open a view page presenting all the available information to the user
- [x] Each movie page must display all its reviews
- [x] Each review should consist of (at least): title, body and stars (1-5)
- [x] The user should be able to create new reviews
- [x] The user should be able to edit their own reviews

## Screenshots

| | |
|----------------|----------------|
|![img](screenshots/Screenshot_1670306921.png)| ![img](screenshots/Screenshot_1670306540.png)|
| | |
|![img](screenshots/Screenshot_1670306996.png)| ![img](screenshots/Screenshot_1670305996.png)|
